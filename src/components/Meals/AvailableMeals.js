import classes from './AvailableMeals.module.css';
import Card from '../UI/Card';
import MealItem from './MealItem/MealItem';

const DUMMY_MEALS = [
    {
        id: 'm1',
        name: 'Sushi',
        description: 'Finest fish and veggies',
        price: 22.99,
    },
    {
        id: 'm2',
        name: 'Schnitzel',
        description: 'A german specialty!',
        price: 16.5,
    },
    {
        id: 'm3',
        name: 'Barbecue Burger',
        description: 'American, raw, meaty',
        price: 12.99,
    },
    {
        id: 'm4',
        name: 'Green Bowl',
        description: 'Healthy...and green...',
        price: 18.99,
    },
    {
        id: 'm5',
        name: 'Peri Fries',
        description: 'Vegetarian',
        price: 8.99,
    },
    {
        id: 'm6',
        name: 'Wings',
        description: 'Flame grilled to perfection',
        price: 13.99,
    },
    {
        id: 'm7',
        name: 'The Wrap',
        description: 'Peri chicken wrapped up in a plain or spinach tortilla with lettuce and tomato',
        price: 15.99,
    },
    {
        id: 'm8',
        name: 'Perimayo Corn',
        description: 'Vegetarian. Hot kernels with a perimayo dip',
        price: 18.99,
    },
    {
        id: 'm9',
        name: 'Grilled Beef',
        description: 'Juicy beef, the best you can get',
        price: 18.99,
    }
];

const AvailableMeals = () => {
    const mealsList = DUMMY_MEALS.map(meal => <MealItem
        id={meal.id}
        key={meal.id}
        name={meal.name}
        description={meal.description}
        price={meal.price}
    />);

    return <section className={classes.meals}>
        <Card>
            <ul>
                {mealsList}
            </ul>
        </Card>
    </section>
};

export default AvailableMeals;